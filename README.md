# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
2. [Primeiros Computadores](capitulos/primeiros_computadores.md)
3. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/Evolução_dos_computadores_pessoais_e_sua_interconexão.md)
    - [Primeira Geração](capitulos/primeira_geração.md)
4. [Computação Móvel](capitulos/computação_movel.md)
4. [Futuro](capitulos/futuro.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11206793/avatar.png?width=400)  | Lucas Alves | @LucasAA12 | [lucasalves.2003@alunos.ultfpr.edu.br](mailto:lucasalves.2003@alunos.ultfpr.edu.br) |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11260557/avatar.png?width=400) | Pedro Marafiga | @PedroMarafiga| [pmarafiga@alunos.utfpr.edu.br](mailto:pmarafiga@alunos.utfpr.edu.br)|
| ![](https://gitlab.com/uploads/-/system/user/avatar/9169642/avatar.png?width=400) | Adrean Souza Rafael  | @adreansouzarafael  |  [adreansousaadreansouzarafael@gmail.com](mailto:adreansousaadreansouzarafael@gmail.com) |
