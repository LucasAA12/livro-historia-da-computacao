# Calculadoras Mecânicas

Os primeiros dispositivos que surgiram para ajudar o homem a calcular têm sua 
origem perdida nos tempos. É o caso, por exemplo, do ábaco. Éra 
capaz de resolver problemas de adição, subtração, multiplicação e divisão de até 12 inteiros, e 
que provavelmente já existia na Babilônia por volta do ano 3.000 a.C. Foi muito utilizado 
pelas civilizações egípcia, grega, chinesa e romana, tendo sido encontrado no Japão, ao 
término da segunda guerra mundial.

Outro exemplo é o compasso de setor, para cálculos trigonométricos, 
utilizado para se determinar a altura para o posicionamento da boca de um canhão, e que foi 
desenvolvido a partir do século XV. 

Exemplo de ábaco:

![](https://s1.static.brasilescola.uol.com.br/be/e/114.jpg)

**Régua de Cálculo**

![](https://img.ibxk.com.br/materias/Slide_rule_cursor.jpg?ims=704x)


Durante vários séculos, o ábaco foi sendo desenvolvido e aperfeiçoado, sendo a principal ferramenta de cálculo por
muito tempo. Entretanto, os principais intelectuais da época do renascimento necessitavam descobrir maneiras mais
eficientes de efetuar cálculos. Logo, em 1638, depois de Cristo, um padre inglês chamado William Oughtred , criou
uma tabela muito interessante para a realização de multiplicações muito grandes. A base de sua invenção foram as
pesquisas sobre logaritmos, realizadas pelo escocês John Napier.

Até este momento, a multiplicação de números muito grandes era algo muito trabalhoso e demorado de ser realizado.
Porém, Napier descobriu várias propriedades matemáticas interessantes e as deu o nome de logaritmos. Após, disso,
multiplicar valores se tornou uma tarefa mais simples.
O mecanismo do William era consistido de um régua que já possuía uma boa quantidade de valores pré-calculados,
organizados em forma que os resultados fossem acessados automaticamente. Uma espécie de ponteiro indicava o
resultado do valor desejado.

**Máquina de Pascal**

Apesar da régua de cálculo de William Oughtred ser útil, os valores presentes nela ainda eram pre-definidos, o que
não funcionaria para calcular números que não estivessem presentes na tábua. Pouco tempo depois, em 1642, o
matemático francês Bleise Pascal desenvolveu o que pode ser chamado da primeira calculadora mecânica da história,
a máquina de Pascal.
Seu funcionamento era baseado no uso de rodas interligadas, que giravam na realização dos cálculos. A ideia inicial
de Pascal era desenvolver uma máquina que realizasse as quatro operações matemáticas básicas, o que não
aconteceu na prática, pois ela era capaz apenas de somar e subtrair. Por esse motivo, ela não foi muito bem acolhida
na época.
Alguns anos após a Máquina de Pascal, em 1672, o alemão Gottfried Leibnitz conseguiu o que pascal não tinha
conseguido, criar uma calculadora que efetuava a soma e a divisão, além da raiz quadrada.

Exemplo: máquina de pascal

![](https://img.ibxk.com.br/materias/maquinapascalina.jpg?ims=704x)

# Referências:

1. FONSECA FILHO, Cléuzio. História da computação: O Caminho do Pensamento e da Tecnologia. EDIPUCRS, 2007.

1. RAMOS, Danielle de Miranda. "Ábaco"; Brasil Escola. Disponível em: https://brasilescola.uol.com.br/matematica/o-abaco.htm. Acesso em 15 de maio de 2022.

1. Gugik, Gabriel. "A história dos computadores e da computação." TecMundo, Curitiba (2009). Disponivel em: https://iow.unirg.edu.br/public/profarqs/2804/0272700/1.A_Historia_dos_computadores_e_da_computacao_-_imprimir.pdf. Acesso em 15 de maio de 2022.
