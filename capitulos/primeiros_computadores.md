# Primeiros computadores

A partir de 1945, após a segunda guerra mundial, uma nova realidade tomava
forma no cenário internacional. Estados Unidos da América (EUA) e União das
Repúblicas Socialistas Soviéticas (URSS), as principais potências emergentes do
conflito, iriam iniciar uma longa disputa pela hegemonia global, denominada Guerra
Fria, disputa essa que ocupou a agenda mundial pelos quase 45 anos seguintes
.
Além desse antagonismo, o período do pós-guerra também foi marcado pela
consolidação de uma nova realidade de produção e armazenamento da informação, com
o aparecimento de novas tecnologias, muitas delas relacionadas aos computadores.
Apesar de modelos analógicos como, por exemplo, os norte-americanos Analisador
Diferencial (1931) e Harvard Mark I (1944), o alemão Z3 (1943), e o inglês Colossus
(1943), terem sido importantes marcos de consolidação dessas tecnologias, a primeira
geração computacional, marcada por equipamentos de grande porte e com considerável
capacidade de processamento de operações, surgiu somente em 1946, obtendo
considerável, e por vezes exagerada e confusa, recepção por parte do público,
organismos governamentais, imprensa e escritores de ficção científica

 
  - **Computador Integrador Numérico Eletrônico – ENIAC**
  
  ![](https://www.cnnbrasil.com.br/wp-content/uploads/sites/12/2021/06/26776_1798DEE935286D54.jpg?w=876&h=484&crop=1)
    
O nascimento do primeiro computador norteamericano surgiu diretamente das necessidades bélicas oriundas da segunda guerra mundial, em especial ligada ao cálculo rápido e eficiente das complexas tabelas de artilharia produzidas pelo exército estadunidense para o conflito na Europa e Ásia. Dispositivos analógicos disponíveis na época, apesar de eficientes, eram insuficientes para diminuir o longo tempo e grande mão de obra gasta para com essas operações.
    
Em junho de 1943, era iniciado o Project PX, projeto secreto do exército americano em conjunto com a Moore School of Electrical Engineering da universidade
    da Pensilvânia, onde se pretendeu criar um equipamento que resolvesse esses problemas
    operacionais sofridos pelas forças armadas norte-americanas.
    Liderado pelos jovens engenheiros John Mauchly (1907-1980) - responsável
    pelo desenho conceitual do equipamento-, e Presper Eckert (1919-1995) – responsável
    pela produção dos circuitos e válvulas-, baseando-se em algoritmos decimais
    identificados em acumuladoras para o desenvolvimento dos cálculos operacionais;
    inspirados no modelo Harvard Mark 1 e da Máquina Analítica de Charles Babbage
    (1791-1871) em sua construção, alguns revezes e 400 mil dólares gastos (superando os
    150 mil inicialmente previstos), em 14 de fevereiro de 1946, era apresentado ao público
    o Computador Integrador Numérico Eletrônico - ENIAC 
    .
    
O computador era composto por 17.468 válvulas, 1.500 relês, canal de
    transmissão, impressora, tinha 25 metros de comprimento por 5,50 metros de altura,
    pesava 30 toneladas e ocupava um grande galpão. Esse modelo, que consumia entre
    150-200 quilowatts, processava 5.000 adições, 357 multiplicações ou 38 divisões por
    segundo. A programação do ENIAC era feita através de 6.000 chaves manuais, onde
    toda a entrada de dados era feita através de cartões de cartolina perfurados (produzidos
    pela empresa IBM), que armazenavam poucas operações cada um. A cada 5 minutos,
    em média, alguma das válvulas se queimava, tornando necessárias manutenções
    frequentes
    .
Apesar dos problemas de manutenção (muitas vezes sendo aproveitado somente
 entre 50 a 70 por cento de sua capacidade), o equipamento, graças, em parte, a
habilidade de Eckert e Mauchly em conseguir apresentar de forma satisfatória o invento
a imprensa, obteve recepção positiva (e de um previsível ceticismo) do público e
organismos militares norte-americanos, sendo o ENIAC rapidamente posto em
 atividade em diferentes projetos ligados, por exemplo, a previsão meteorológica,
construção de túneis de vento, física nuclear e da matéria. Essas utilizações
diversificadas permitiram a discussão de alguns questionamentos que serviriam de base
 para a evolução dos computadores (ou “cérebros gigantes”, como eram apresentados na
 época) estadunidenses durante a segunda metade dos anos 1940.
    
Por fim, o ENIAC estimulou pesquisas com o objetivo das novas tecnologias de
informação terem sua produção e utilização expandidas do contexto militar, sendo
 inseridas também nas empresas privadas estadunidenses. Modelos como o BINAC
(1949) e o UNIVAC-1 (1951),- ambos desenvolvidos pela Eckert–Mauchly Computer
 Corporation, empresa de curta duração criada pelos criadores do ENIAC, fundida com a
Remington Rand em 1950 - esse último inserido em diferentes instituições públicas e
 privadas dos EUA, mostraram o potencial comercial dos computadores, que
estimularam o desenvolvimento de novos e mais eficientes modelos, além de servirem
de estímulo para disciplinas emergentes como, por exemplo, a Cibernética, em
realizarem seus estudos e identificarem suas principais características.

   
 # Referências
 *Nos primórdios da informática: estudo sobre a construção dos primeiros
computadores eletrônicos digitais nos Estados Unidos e União Soviética*
Roberto Lopes dos Santos Junior1, artigo bobb UERJ, 2015
