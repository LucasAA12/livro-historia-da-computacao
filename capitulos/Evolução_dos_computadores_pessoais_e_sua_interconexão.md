# Evolução dos computadores pessoais e sua interconexão

# Evolução dos computadores pessoais
Somente na Quarta geração que os computadores começaram a ser de uso pessoal, com os VLSI(Integração de Circuitos em Escala Muito Alta)  o desempenho aumentou e o custo caiu e surgem os modelos de grandes companhias como o:
-IBM PC - Personal Computer (IBM, 1981)  Intel 8088  Projeto de circuitos público  
Objetivo: permitir que outros fabricassem componentes facilmente acopláveis ao PC.
  Consequência: indústria de clones.  Sistema operacional: MS-DOS. Computador mais vendido de toda a história.
Surge também o Windows, conceitos das arquiteturas RISC(Reconhece um número limitado de instruções que, em contrapartida, são otimizadas para que sejam executadas com mais rapidez) e processadores superescalares.
Após a virada do milênio, os computadores continuam seguindo a tendência de miniaturização de seus componentes, tornando os computadores mais escaláveis e práticos nas tarefas diárias. Além disso, há um grande investimento em seu design.
Além do IBM PC alguns outros computadores marcaram o início da quarta geração, sendo eles:  

Altair 8800

![](https://files.tecnoblog.net/meiobit/wp-content/uploads/2021/01/1280px-Altair_8800_Smithsonian_Museum.jpg)

Seu projeto terminou em 1974 e começou a ser comercializado no ano seguinte. Os criadores da linguagem básica decidiram licenciar para ser aquele que esta máquina carregava como padrão (incluindo "Altair BASIC", um intérprete de linguagem desenvolvido pela Microsoft). Il compreendido também o Intel 8080, que seria o primeiro microprocessador de 16 bits. O barramento integrado (S-100) foi o padrão nos anos seguintes .
Sua venda com teclado e mouse foi um sucesso e porque muitos o consideram o primeiro computador pessoal. Além disso, gostaram muito que seja vendido em versão montada, mas também que se apresente para ser montado em kit, com mais de uma centena de opções.

Apple II
![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Apple-II.jpg/250px-Apple-II.jpg)

Uma família de microcomputadores produzidos em série em 1977 . Este está sendo vendido há 7 anos com grande sucesso e a um preço mais do que razoável, principalmente após o lançamento da planilha VisiCalcGenericName.
O original montava um processador 6502 , 4 KiB de RAM e sua arquitetura era de 8 bits. Em 1979 surgiu o Apple II Plus, notável por poder expandir sua memória RAM com um "cartão de idioma".

# Interconexão
  Na década de 70 com a necessidade de um incremento na capacidade de processamento e armazenamento surgiu a necessidade de um desempenho melhor e surge a necessidade de desenvolver técnicas para interconexão de  computadores.

Nas empresas modernas temos grande quantidade de computadores operando em diferente setores. - Operação do conjunto mais eficiente se estes computadores forem interconectados: - possível compartilhar recursos - possível trocar dados entre máquinas de forma simples e confortável para o operador - vantagens gerais de sistemas distribuídos e downsizing atendidas - Redes são muito importantes para a realização da filosofia CIM (Manufatura Integrada por Comput.).

LAN (Local Area Network) ou Rede Local Industrial : interconexão de computadores localizados em uma mesma sala ou em um mesmo prédio. Extensão típica: até aprox. 200 m.
 CAN (Campus Area Network): interconexão de computadores situados em prédios diferentes em um mesmo campus ou unidade fabril. Extensão típica: até aprox. 5 Km.
 MAN (Metropolitan Area Network): interconexão de computadores em locais diferentes da mesma cidade. Pode usar rede telefônica pública ou linha dedicada. Extensão típica: até aprox. 50 Km. 
WAN (Wide Area Network) ou Rede de Longa Distância: interconexão de computadores localizados em diferentes prédios em cidades distantes em qualquer ponto do mundo. Usa rede telefônica, antenas parabólicas, satélites, etc. Extensão >50 Km.
  

# Referência
 Andrew S. Tanenbaum, Organização Estruturada de Computadores, Capítulo 1, 5ª edição, Prentice-Hall do Brasil, 2007.
 Lúcia Helena M. Pacheco, Visão Geral de Organização Estruturada de Computadores e Linguagem de Montagem. Universidade Federal de Santa Catarina. Centro Tecnológico, Departamento de Informática e de Estatística. 
 http://www.inf.ufsc.br/~lucia/Arquivos-INE5607/Turma0238B/OrgEstruturada.pdf

“Computer Networking: A Top-Down Approach Featuring the Internet” James Kurose e Keith Ross http://occawlonline.pearsoned.com/bookbind/pubbooks/kurose-ross1/
