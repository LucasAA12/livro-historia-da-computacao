# Futuro

Visto de um ponto de vista do presente
– exatamente no momento em que lemos
este texto – o futuro é uma ficção. Mesmo
assim é possível discorrer a respeito dessa
ficção dada a nossa experiência acumulada
no correr dos anos e a nossa imaginação.
A combinação dessa experiência e imaginação nos permite criar um discurso.
Dependendo de como esses dois elementos
são combinados, obtemos uma previsão
realista e com grande possibilidade de
ser realizada ou simplesmente uma obra
da imaginação.
Existe uma diferença básica entre o futuro próximo, a saber, “em alguns meses ou
anos”, e o futuro remoto, como “nos séculos
vindouros”. Num futuro próximo é possível
prever como um certo tipo de tecnologia,
ou estado de coisas, evolve levando-se em
conta que um certo número de condições são
satisfeitas. Por exemplo, o desenvolvimento
da tecnologia de comunicação pessoal, tal
como o telefone móvel, nos próximos meses
ou anos vai tomar a forma da integração de
várias tecnologias num único sistema. O
sucesso da previsão desse tipo de tecnologia
depende do fato de que essas condições não
sejam alteradas.

O que se seguiu nos anos 90 até os nossos
dias foi o estabelecimento da Internet como
standard para comunicação de informação
sonora, visual e textual usado desde um meio
de comunicação pessoal até o comércio.
Mas quem poderia prever esse “futuro”
da Internet nos seus primórdios na década
de 60, e mesmo 15 anos atrás, como um
fato standard de comunicação? Podemos
dizer que ninguém. O desenvolvimento
da Internet como a conhecemos hoje levou aproximadamente 40 anos, o que, em
termos da velocidade do desenvolvimento
da chamada alta tecnologia, representa um
longo período; poderíamos dizer que hoje,
2007, é um “futuro distante” comparado
aos anos 60. Então a definição de “futuro”
depende também de que tipo de tecnologia
estamos falando.

Mas como se desdobra o desenvolvimento (evolução) do presente para o futuro?
Mesmo se o intervalo de tempo entre o
presente e o futuro for extremamente curto
– infinitesimal –, o futuro não coincidirá
com o presente. Os diferentes possíveis
“futuros” são descritos pelos diferentes
modos desse desenvolvimento. Do ponto de
vista formal, o modelamento desse processo
de desenvolvimento é extremamente complexo.

Alguns aspectos da alta
tecnologia do futuro
 
 - **Miniaturização**

A miniaturização já tem sido desenvolvida nos últimos 50 anos, em particular
com o rápido desenvolvimento da tecnologia de chips. Nos últimos 20 anos a
nanotecnologia tem se firmado como nova
área da engenharia que combina resultados da física dos materiais, da química,
da biologia e das engenharias elétrica e
mecânica, para a criação de novos materiais e sistemas baseados em processos na
escala de 10-9 metros (nanômetros). Esse
aspecto abre as portas de um conjunto
enorme de novas aplicações que vamos
discutir a seguir.

 
 - **Artificialidade**

A artificialidade se refere a novos tipos
de entidades sintéticas, que são reais ou
“virtuais”. A característica principal de uma
entidade artificial é que ela não foi criada
por meios naturais; a interação com essa
entidade precisa ser mediada por algum
dispositivo. Com o advento da miniaturização do computador e da inteligência
artificial foi possível a implementação de
entidades artificiais.

 
 - **Automatização**

A automatização é uma tendência da
tecnologia que tem se acelerado nos últimos 100 anos. Isso é evidente nas linhas de
produção de bens materiais os mais diversos
em que é necessário otimizar a qualidade,
o tempo de produção e a capacidade de
reprodutibilidade de um dado produto.

 
 - **Hibridização**

A hibridização combina métodos,
meios e materiais tendo como meta a
criação de sistemas sintéticos. Recentes
avanços da engenharia eletrônica, da
biologia e da computação possibilitaram
a implementação dos primeiros sistemas
de biologia sintética.
A seguir vamos discutir em mais detalhes as três áreas com grande potencial de
desenvolvimento no futuro, analisando a
influência dos aspectos descritos acima.


 - **Robótica**

A robótica tem tido um grande desenvolvimento nos últimos 100 anos graças
à evolução de um conjunto de áreas da
engenharia e da ciência. A robótica é, por
excelência, um ramo da engenharia do
futuro. Os principais ramos da engenharia
que contribuíram para a robótica são as
engenharias mecânica e eletrônica. Principalmente desde os séculos XVII e XVIII
houve uma revolução no desenvolvimento
de sistemas totalmente mecanizados, o que
era importante para a economia e a eficiência
de produção da era industrial daquela época.
Em particular, com o desenvolvimento da
mecânica de alta precisão, foram desenvolvidos sistemas autômatos, como relógios,
bonecos e instrumentos musicais. O desenvolvimento da eletrônica só ocorreu de
forma mais acentuada com a miniaturização
dos circuitos eletrônicos.
Aquilo que diferencia a robótica dos
desenvolvimentos anteriores dos sistemas
mecânico-eletrônicos são o uso de sensores
sofisticados e a inteligência artificial. Em
particular, o uso de métodos da inteligência artificial propicia o desenvolvimento
de sistemas autônomos.


## Conclusão

Discursar sobre o futuro da tecnologia
é possível. Para tal é necessário combinar
informação sobre o desenvolvimento passado de uma dada tecnologia, como, por
exemplo, a televisão, com necessidades
de mercado, desenvolvimentos sociais e
tendências gerais do consumidor. O que é
mais difícil é prever o futuro: isso requer
um grau de exatidão que muitas vezes é
difícil de gerar. Prever significa que certas
características de uma tecnologia, dadas
pequenas variações em torno delas, vão ser
concretizadas. Uma vez que essas características não sejam confirmadas no futuro, a
previsão falha. Muitas vezes os elementos de
uma tecnologia criada num dado momento
histórico podem ser conhecidos antes desse
momento – meses, anos – e, mesmo assim,
essa tecnologia pode não se materializar pela
falta de condições de mercado ou mesmo de
conhecimento adequado de como combinar
esses elementos para realizar essa tecnologia.
Um exemplo disso é a Internet: quem poderia ter previsto a Internet de hoje, que entre
outras coisas deu condições para a criação de
uma forma totalmente nova de comércio em
âmbito internacional, bem como uma vasta
gama de serviços e informação. Isso mostra
a importância dos aspectos econômicos, políticos e sociais no desenvolvimento de uma
dada tecnologia. De um lado, esses aspectos
criam condições para o aparecimento de novas tecnologias; de outro lado, essas novas
tecnologias podem alterar esses aspectos,
muitas vezes de forma radical.
O desenvolvimento de diferentes elementos de uma tecnologia se dá em paralelo
ou em cadeia. O que torna difícil prever o
futuro de uma dada tecnologia é que não é
possível ter uma idéia precisa de: i) como
seus elementos vão se desenvolver e se
tornar maduros para seu uso; ii) como esses
elementos vão ser combinados. Dependendo
de como se combinam, conceptualmente,
esses elementos, obtém-se um diferente
futuro para uma dada tecnologia. Dessa
forma, prever o futuro significa criar uma
dada representação de um possível futuro.
Prevê-se por projeção ao futuro daquilo que
se sabe sobre o presente e o passado de uma
dada tecnologia.

![](https://distrito.me/wp-content/uploads/2021/05/Distrito-O-Futuro-da-Midia.jpg)

## Referência


"*Sobre o futuro da tecnologia*" RADU S. JASINSCHI, REVISTA USP, São Paulo, n.76, p. 6-25, dezembro/fevereiro 2007-2008
