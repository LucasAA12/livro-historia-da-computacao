# Primeira geração

A fase inicial, compreendida entre os anos 40 e 50 do século XX, tem como
característica principal a busca pelo aumento da potência de cálculo dos computadores
para finalidades militares. Desde a Antigüidade, a humanidade teve como uma de suas
principais preocupações a busca por instrumentos de cálculo cada vez mais eficientes.
Estes instrumentos podem ser divididos em dois grupos: os que atendem ao processo
analógico e os que atendem ao processo numérico (ou digital). Um exemplo do primeiro
grupo é a régua, enquanto o grande exemplo do segundo grupo é o computador.
Referindo-se ao seu processo de cálculo, Carità (1987, p. 283) afirma que “com ele o
cálculo não é mais realizado numa base decimal, o mais comum e talvez mais antigo
sistema de cálculo, derivado do uso dos dez dedos, mas utiliza um sistema binário de
cálculo, ou seja, baseado em apenas dois algarismos:  ́0 e  ́1.”
Assim surgiram os primeiros computadores, nos anos 40 do século XX, tanto
nos Estados Unidos da América (EUA) quanto na Inglaterra: grandes máquinas de calcular para usos militares. A Segunda Guerra Mundial e a posterior Guerra Fria foram
cruciais neste processo calcular para usos militares. A Segunda Guerra Mundial e a posterior Guerra Fria foram cruciais neste processo.
Os EUA lideraram este processo e, como o grande motivo do surgimento dos
computadores não foi comercial (mas sim a otimização de cálculos militares durante a
Segunda Guerra), somente algumas décadas depois é que os computadores se tornaram
bens de consumo. Segundo Lévy (1999, p. 31), “a informática servia aos cálculos
científicos, às estatísticas dos Estados e das grandes empresas ou a tarefas pesadas de
gerenciamento (folhas de pagamento etc.)”. Para que os computadores viessem a se
tornar objetos de consumo pessoal, o desenvolvimento de alguns dispositivos
tecnológicos nos anos subsequentes desempenhou um papel fundamental. Como por exemplo os transistores(com a mesma função de valvulas mas sem ocupar tanto espaço e consumindo cem mil vezes menos energia)e os chips.

# MARK 1 – 1944
Os primeiros computadores eletrônicos surgiram na mesma época na Alemanha, Inglaterra e nos Estados Unidos, lar do Mark 1. Com 4,5 toneladas, ele demorava seis segundos numa multiplicação.

![](https://time.graphics/uploadedFiles/500/fe/22/fe22e9f3b88f748c48380f33cf471e10.jpg)

# ENIAC – 1946
Esse Eniac foi o primeiro computador “multiuso” – os anteriores apenas desempenhavam tarefas específicas, como cálculos de bombardeios em guerra.

![](https://files.tecnoblog.net/wp-content/uploads/2011/02/785px-Eniac.jpg)

# TRANSISTOR – 1947
Os primeiros computadores usavam componentes enormes, as válvulas, para guardar informação e fazer contas. O transistor fazia a mesma coisa, mas era menor, mais barato e consumia menos energia.

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRh9PsApczAjAp5xiPFT34CXx_-8gZ43zIdWA&usqp=CAU)

# LEO – 1951
As primeiras máquinas foram usadas na guerra e nas universidades. Isso até a chegada do Leo, empregado numa empresa inglesa de alimentos, a primeira a usar um computador nos negócios.

![](https://tm.ibxk.com.br/2011/11/materias/1549318121821.jpg?ims=1200x675)

# WHIRLWIND – 1953
Esse computador era o mais rápido do seu tempo: ele fazia uma multiplicação em 0,025 segundo. Mas ainda era uma carroça, equivalente, hoje, à velocidade de um processador de 1 MHz.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Museum_of_Science%2C_Boston%2C_MA_-_IMG_3168.JPG/1200px-Museum_of_Science%2C_Boston%2C_MA_-_IMG_3168.JPG)

# TRADIC – 1955
Feito para a Força Aérea Americana, esse foi o primeiro computador a usar transistores, que tornaram as máquinas rápidas, leves e econômicas.

![](https://upload.wikimedia.org/wikipedia/commons/b/b5/TRADIC_computer.jpg)

# Referências 

http://www.ufrgs.br/alcar/encontros-nacionais-1/encontros-nacionais/6o-encontro-2008-1/Breve%20historia%20dos%20computadores%20e%20do%20ciberespaco.pdf


